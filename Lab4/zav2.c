#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

typedef int elemtype;
struct elem {
    elemtype* value;
    struct elem* next;
    struct elem* prev;
};

struct List {
    struct elem* head;
    struct elem* tail;
    size_t size;
};

typedef struct elem Node;
typedef struct List myList;

myList* createList(void) {
    myList* list = (myList*)malloc(sizeof(myList));
    if (list) {
        list->size = 0;
        list->head = list->tail = NULL;
    }
    return list;
}

void deleteList(myList* list) {
    Node* head = list->head;
    Node* next = NULL;
    while (head) {
        next = head->next;
        free(head);
        head = next;
    }
    free(list);
    list = NULL;
}

bool isEmptyList(myList* list) {
    return ((list->head == NULL) || (list->tail == NULL));
}

int pushFront(myList* list, elemtype* data) {
    Node* node = (Node*)malloc(sizeof(Node));
    if (!node) {
        return(-1);
    }
    node->value = data;
    node->next = list->head;
    node->prev = NULL;
    if (!isEmptyList(list)) {
        list->head->prev = node;
    }
    else {
        list->tail = node;
    }
    list->head = node;
    list->size++;
    return(0);
}

int popFront(myList* list, elemtype* data) {
    Node* node;
    if (isEmptyList(list)) {
        return(-2);
    }
    node = list->head;
    list->head = list->head->next;
    if (!isEmptyList(list)) {
        list->head->prev = NULL;
    }
    else {
        list->tail = NULL;
    }
    *data = *(node->value);
    free(node);
    list->size--;
    return(0);
}

int pushBack(myList* list, elemtype* data) {
    Node* node = (Node*)malloc(sizeof(Node));
    if (!node) {
        return(-3);
    }
    node->value = data;
    node->next = NULL;
    node->prev = list->tail;
    if (!isEmptyList(list)) {
        list->tail->next = node;
    }
    else {
        list->head = node;
    }
    list->tail = node;
    list->size++;
    return(0);
}

int popBack(myList* list, elemtype* data) {
    Node* node = NULL;
    if (isEmptyList(list)) {
        return(-4);
    }
    node = list->tail;
    list->tail = list->tail->prev;
    if (!isEmptyList(list)) {
        list->tail->next = NULL;
    }
    else {
        list->head = NULL;
    }
    *data = *(node->value);
    free(node);
    list->size--;
    return(0);
}

Node* getNode(myList* list, int index) {
    Node* node = NULL;
    int i;
    if (index >= list->size) {
        return (NULL);
    }
    if (index < list->size / 2) {
        i = 0;
        node = list->head;
        while (node && i < index) {
            node = node->next;
            i++;
        }
    }
    else {
        i = list->size - 1;
        node = list->tail;
        while (node && i > index) {
            node = node->prev;
            i--;
        }
    }
    return node;
}

Node* getFirst(myList* list) {
    if (isEmptyList(list)) {
        return NULL;
    }
    return list->head;
}

Node* getLast(myList* list) {
    if (isEmptyList(list)) {
        return NULL;
    }
    return list->tail;
}

void printValue(elemtype* value) {
    printf("%d ", *value);
}

void printList(myList* list, void(*func)(elemtype*)) {
    Node* node = list->head;
    if (isEmptyList(list)) {
        return;
    }
    while (node) {
        func(node->value);
        node = node->next;
    }
    printf("\n");
}

int main() {
    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №2 лабораторної роботи №4\n");

    myList* list = createList();
    if (isEmptyList(list)) {
        printf("Список порожній\n");
    }

    int option;
    elemtype data;

    while (true) {
        printf("\nОберіть опцію:\n");
        printf("1. Додати елемент\n");
        printf("2. Видалити елемент\n");
        printf("3. Вивести список\n");
        printf("4. Завершити програму\n");
        printf("Ваш вибір: ");
        scanf("%d", &option);

        switch (option) {
            case 1:
                printf("Введіть значення елемента: ");
                scanf("%d", &data);
                pushBack(list, &data);
                printf("Елемент успішно доданий до списку.\n");
                break;
            case 2:
                if (isEmptyList(list)) {
                    printf("Список порожній.\n");
                } else {
                    popBack(list, &data);
                    printf("Видалений елемент: %d\n", data);
                }
                break;
            case 3:
                printf("Список: ");
                printList(list, printValue);
                break;
            case 4:
                deleteList(list);
                printf("Програма завершена.\n");
                return 0;
            default:
                printf("Невірний вибір. Спробуйте ще раз.\n");
                break;
        }
    }

    return 0;
}
