#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <string.h>

typedef struct Node {
    double data;
    struct Node* next;
} Node;

typedef struct {
    Node* top;
} Stack;

void initStack(Stack* stack) {
    stack->top = NULL;
}

int StackEmpty(Stack* stack) {
    return (stack->top == NULL);
}

void push(Stack* stack, double value) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    if (newNode == NULL) {
        printf("Помилка виділення пам'яті!\n");
        exit(1);
    }
    newNode->data = value;
    newNode->next = stack->top;
    stack->top = newNode;
}

double pop(Stack* stack) {
    if (StackEmpty(stack)) {
        printf("Стек порожній!\n");
        exit(1);
    }
    Node* topNode = stack->top;
    double value = topNode->data;
    stack->top = topNode->next;
    free(topNode);
    return value;
}

int isNumeric(char* token) {
    for (int i = 0; i < strlen(token); i++) {
        if (!isdigit(token[i]) && token[i] != '.') {
            return 0;
        }
    }
    return 1;
}

double check(char* expression) {
    Stack stack;
    initStack(&stack);

    char* token = strtok(expression, " ");
    while (token != NULL) {
        if (isNumeric(token)) {
            double operand = atof(token);
            push(&stack, operand);
        } else {
            if (StackEmpty(&stack)) {
                printf("Недостатня кількість операндів у виразі!\n");
                exit(1);
            }

            double x2 = pop(&stack);

            if (StackEmpty(&stack)) {
                printf("Недостатня кількість операндів у виразі!\n");
                exit(1);
            }

            double x1 = pop(&stack);

            double result = 0.0;

            switch (token[0]) {
                case '+':
                    result = x1 + x2;
                    break;
                case '-':
                    result = x1 - x2;
                    break;
                case '*':
                    result = x1 * x2;
                    break;
                case '/':
                    result = x1 / x2;
                    break;
                case '^':
                    result = pow(x1, x2);
                    break;
                case 's':
                    result = sqrt(x2);
                    break;
                default:
                    printf("Недійсний оператор = %c!\n", token[0]);
                    exit(1);
            }

            push(&stack, result);
        }

        token = strtok(NULL, " ");
    }

    if (stack.top == NULL || stack.top->next != NULL) {
        printf("Недійсний вираз!\n");
        exit(1);
    }

    double result = pop(&stack);
    return result;
}


int main() {
    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №3 лабораторної роботи №4\nВведіть арифметичний вираз у зворотній польській нотації = ");

    char expression[100];
    fgets(expression, sizeof(expression), stdin);

    if (expression[strlen(expression) - 1] == '\n') {
        expression[strlen(expression) - 1] = '\0';
    }

    double result = check(expression);
    printf("Результат = %lf\n", result);

    return 0;
}
