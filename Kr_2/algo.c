#include <stdio.h>
#include <stdlib.h>

struct Edge {                                                                                        // Структура ребра графу
    int source, destination, weight;
};

struct Graph {                                                                                       // Структура графу
    int numVertices, numEdges;
    struct Edge* edges;
};

struct Graph createGraph(int numVertices, int numEdges) {                                            // Функція для створення графу
    struct Graph graph;
    graph.numVertices = numVertices;
    graph.numEdges = numEdges;
    graph.edges = (struct Edge*)malloc(numEdges * sizeof(struct Edge));
    return graph;
}

struct Subset {                                                                                      // Структура для представлення множини з компонентами зв'язності
    int parent;
    int rank;
};

int find(struct Subset subsets[], int i) {                                                           // Функція для знаходження кореня підмножини
    if (subsets[i].parent != i)
        subsets[i].parent = find(subsets, subsets[i].parent);
    return subsets[i].parent;
}

void unionSets(struct Subset subsets[], int x, int y) {                                              // Функція для об'єднання двох підмножин залежно від рангу
    int xroot = find(subsets, x);
    int yroot = find(subsets, y);
    if (subsets[xroot].rank < subsets[yroot].rank)
        subsets[xroot].parent = yroot;
    else if (subsets[xroot].rank > subsets[yroot].rank)
        subsets[yroot].parent = xroot;
    else {
        subsets[yroot].parent = xroot;
        subsets[xroot].rank++;
    }
}

int compare(const void* a, const void* b) {                                                          // Функція для порівняння ребер за їх вагою для сортування
    struct Edge* edgeA = (struct Edge*)a;
    struct Edge* edgeB = (struct Edge*)b;
    return edgeA->weight - edgeB->weight;
}


void funkruskal(struct Graph graph) {                                                                // Функція для виконання алгоритму Крускала та виведення мінімального кістякового дерева
    int numVertices = graph.numVertices;
    struct Edge result[numVertices];                                                                 // Результуюче мінімальне кістякове дерево
    int e = 0;                                                                                       // Індекс для result[]
    int i = 0;
    qsort(graph.edges, graph.numEdges, sizeof(struct Edge), compare);                                // Сортування ребер графу за зростанням ваги

    struct Subset* subsets = (struct Subset*)malloc(numVertices * sizeof(struct Subset));            // Виділення пам'яті для збереження підмножин компонент зв'язності

    for (int v = 0; v < numVertices; v++) {                                                          // Ініціалізація підмножин та їх рангів
        subsets[v].parent = v;
        subsets[v].rank = 0;
    }

    while (e < numVertices - 1 && i < graph.numEdges) {                                              // Поки не досягнуто numVertices - 1 ребер, обираємо найлегше ребро та перевіряємо цикл
        struct Edge nextEdge = graph.edges[i++];

        int x = find(subsets, nextEdge.source);
        int y = find(subsets, nextEdge.destination);

        if (x != y) {                                                                                // Якщо додавання ребра не створює цикл, додаємо його до результату
            result[e++] = nextEdge;
            unionSets(subsets, x, y);
        }
    }

    printf("\nМінімальне кістякове дерево:\n");                                                        // Виведення мінімального кістякового дерева
    for (i = 0; i < e; i++) {
        printf("%d - %d, Вага: %d\n", result[i].source, result[i].destination, result[i].weight);
    }

    free(subsets);                                                                                   // Звільнення пам'яті
}

int main() {
    int NumVertices, NumEdges;

    printf("Виконав Білий Микола студент групи ІПЗ 22-2\nВведіть кількість вершин: ");
    scanf("%d", &NumVertices);
    printf("Введіть кількість ребер: ");
    scanf("%d", &NumEdges);

    struct Graph graph = createGraph(NumVertices, NumEdges);                                         // Додавання ребер графу 
                                                                                                     
    printf("Введіть деталі ребер графу:\n");
    for (int i = 0; i < NumEdges; i++) {
        printf("\nРебро %d:\n", i + 1);
        printf("\nДжерело: ");
        scanf("%d", &graph.edges[i].source);
        printf("Призначення: ");
        scanf("%d", &graph.edges[i].destination);
        printf("Вага: ");
        scanf("%d", &graph.edges[i].weight);
    }

    funkruskal(graph);

    return 0;
}