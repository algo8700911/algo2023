#include <stdio.h>
#include <time.h>

unsigned long long factorial(int a) {
    if (a == 0 || a == 1) return 1;
    else return a * factorial(a - 1);
}

int main() {
    int a;

    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №2 лабораторної роботи №3\nВведіть ціле число a (0 ≤ a ≤ 20): ");
    scanf("%d", &a);

    if (a < 0 || a > 20) {
        printf("Введене число не входить у допустимий діапазон!\n");
        return 1;
    }

    clock_t start = clock();  

    unsigned long long result = factorial(a);

    clock_t end = clock();  
    double cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("Факторіал числа a = %llu\n", result);
    printf("Час виконання програми: %f секунд\n", cpu_time_used);

    return 0;
}
