#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int generator(int min, int max) {
    return (rand() % (max - min + 1)) + min;
}

int comparisonfun(const void* a, const void* b) {
    return *(int*)b - *(int*)a;
}

unsigned long long LarNumber(int number[], int size) {
    qsort(number, size, sizeof(int), comparisonfun);

    unsigned long long larNum = 0;
    unsigned long long mnozhnik = 1;

    for (int i = 0; i < size; i++) {
        larNum += number[i] * mnozhnik;
        mnozhnik *= 8;
    }

    return larNum;
}

int main() {
    srand(time(0));

    int n;
    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №3 лабораторної роботи №3\nВведіть обсяг масиву (n ≤ 20): ");
    scanf("%d", &n);

    if (n < 1 || n > 20) {
        printf("Введений обсяг масиву не відповідає допустимому діапазону.\n");
        return 1;
    }

    int number[n];
    unsigned long long larNum;
    
    clock_t start = clock();  
    
    larNum = LarNumber(number, n);
    
    clock_t end = clock();  
    double cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    printf("Згенерований масив цифр вісімкової системи числення: ");
    for (int i = 0; i < n; i++) {
        number[i] = generator(0, 7);
        printf("%d ", number[i]);
    }

    printf("\nНайбільше можливе число: %llu\n", larNum);
    printf("Час виконання програми: %f секунд\n", cpu_time_used);

    return 0;
}
