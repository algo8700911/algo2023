#include <stdio.h>
#include <math.h>

#define maxN 50
#define step 1
#define maxY 500

double fun1(int n) {
    return (double)n;
}

double fun2(int n) {
    return log(n);
}

double fun3(int n) {
    return (double)n * log(n);
}

double fun4(int n) {
    return (double)(n * n);
}

double fun5(int n) {
    return pow(2, n);
}

double factorial(int n) {
    if (n == 0)
        return 1;
    else
        return n * factorial(n - 1);
}

double fun6(int n) {
    return factorial(n);
}

int main() {

    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №1 лабораторної роботи №3\nФункція №1\n");
    for (int n = 1; n <= maxN; n += step) {
        double y1 = fun1(n);
        printf("%lf\n", y1);
    }

    printf("\nФункція №2\n");
    for (int n = 1; n <= maxN; n += step) {
        double y2 = fun2(n);
        printf("%lf\n", y2);
    }

    printf("\nФункція №3\n");
    for (int n = 1; n <= maxN; n += step) {
        double y3 = fun3(n);
        printf("%lf\n", y3);
    }

    printf("\nФункція №4\n");
    for (int n = 1; n <= maxN; n += step) {
        double y4 = fun4(n);
        printf("%lf\n", y4);
    }

    printf("\nФункція №5\n");
    for (int n = 1; n <= maxN; n += step) {
        double y5 = fun5(n);
        printf("%lf\n", y5);
    }

    printf("\nФункція №6\n");
    for (int n = 1; n <= maxN; n += step) {
        double y6 = fun6(n);
        printf("%lf\n", y6);
    }
    
    return 0;
}