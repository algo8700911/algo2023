#include <stdio.h> 
#include <math.h>

unsigned long long x = 59400; 
unsigned long long a = 1664525;  
unsigned long long c = 1013904223; 
unsigned long long m = 4294967296; 

unsigned long long rand() { 
    x = (a * x + c) % m; 
    return x % 150; 
} 

int main() 
{ 
    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №1 лабораторної роботи №2\n");


    unsigned long long arr[20000], interval[20000] = {0}, sum;
    int i, j, k, f, F = 0;
    float p, var; 
    double mats, sqrtvid;
    for(i = 0; i < 20000; i++) { 
        arr[i] = rand();
        printf("%llu", arr[i]); 
        if(i < 19999) 
        {
            printf("   ");
        }
        else 
        {
            printf("\n");
        }
    }
    
    for (i = 0; i < 20000; i++) {
        f = 0;
        for (j = i + 1; j < 20000; j++) {
            if (arr[i] == arr[j]) 
            {
                F++;
                f++;
                break; 
            }
        }
        if (f > 0) printf("\nКількість повторень значення %llu = %d\n", arr[i], f);
    }
    if (F == 0) printf("\nБез повторень");
    
    for (i = 0; i < 150; i++) {
        k = 0;
        for (j = 0; j < 20000; j++) {
            if (arr[j] == i) 
            {
                k++;
            }
        }
        p = (float) k / 1000;
        printf("Статистична імовірність появи випадкових величин %d = %f\n", i, p);
    }

    for (int i = 0; i < 20000; i++) {
        sum += arr[i];
    }
    mats = sum / 20000;
    printf("Математичне сподівання випадкових величин = %f\n", mats);
    
    for (int i = 0; i < 20000; i++) {
        var += pow(arr[i] - mats, 2);
    }
    var /= 20000;
    printf("Дисперсія випадкових величин = %f\n", var);
    

    sqrtvid= sqrt(var);
    printf("Cередньоквадратичне відхилення випадкових величин = %f\n", sqrtvid);

    return 0; 
}