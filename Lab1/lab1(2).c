#include <stdio.h>
#include <stdint.h>
#include <math.h>

union Union
{
    struct
    {
        uint16_t b0 : 1;
        uint16_t b1 : 1;
        uint16_t b2 : 1;
        uint16_t b3 : 1;
        uint16_t b4 : 1;
        uint16_t b5 : 1;
        uint16_t b6 : 1;
        uint16_t b7 : 1;
        uint16_t b8 : 1;
        uint16_t b9 : 1;
        uint16_t b10 : 1;
        uint16_t b11 : 1;
        uint16_t b12 : 1;
        uint16_t b13 : 1;
        uint16_t b14 : 1;
        uint16_t b15 : 1;
    } Struct;
    int16_t Num;
};

int main()
{
    signed short a, b = 0;
    union Union p;
    int Numpidzav;
    char znak;

    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №2 лабораторної роботи №1\nОбреіть номер бажаного підзавдання (1 або 2): ");
    scanf("%d", &Numpidzav);
    switch (Numpidzav)
    {
    case 1:
        printf("Ви обрали підзавдання під номером 1\nВикористання структур даних та об'єднань\nВведіть ціле число типу signed short (-32768...32767): ");
        scanf("%hd", &a);
        p.Num = a;
        int arr[] = {p.Struct.b0, p.Struct.b1, p.Struct.b2, p.Struct.b3, p.Struct.b4, p.Struct.b5, p.Struct.b6, p.Struct.b7, p.Struct.b8, p.Struct.b9, p.Struct.b10, p.Struct.b11, p.Struct.b12, p.Struct.b13, p.Struct.b14};
        if (p.Struct.b15 == 1)
        {
            znak = '-';
            for (int i = 0; i < 15; i++)
            {
                if (arr[i] == 0)
                {
                    arr[i] = 1;
                }
                else 
                {
                    arr[i] = 0;
                }
            }
            for (int i = 0; i < 15; i++)
            {
                b += (arr[i] * pow(2, i));
            }
            b++;
            printf("Введене значення = %c%d\n", znak, b);
        }
        else 
        {
            znak = '+';
            for (int i = 0; i < 15; i++)
            {
                b += (arr[i] * pow(2, i));
            }
            printf("Введене значення = %c%d\n", znak, b);
        }
        break;

    case 2:
        printf("Ви обрали підзавдання під номером 2\nВикористання побітових логічних операцій\nВведіть ціле число типу signed short (-32768...32767): ");
        scanf("%hd", &a);
        b = (a ^ (a >> 15)) - (a >> 15);
        znak = (a >> 15) ? '-' : '+';
        printf("Введене значення = %c%d\n", znak, b);
        break;

    default: printf("Ви ввели неправильний номер підзавдання !\n");
        break;
    }

    return 0;
}