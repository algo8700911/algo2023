#include <stdio.h>
#include <stdlib.h>

int main()
{
    signed char a = 5, b = 127, y, c = 2, d = 3, e = -120, f = -34, z, g = -5, h = 56, j = 38;

    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №3 лабораторної роботи №1\n");
    y = a + b;
    z = e + f;
    printf("а) 5 + 127 = %d\n", y);
    printf("б) 2 - 3 = %d\n", c - d);
    printf("в) - 120 - 34 = %d\n", z);
    printf("г) (unsigned char) (- 5) = %d\n", (unsigned char)g);
    printf("д) 56 & 38 = %d\n", h & j);
    printf("е) 56 | 38 = %d\n", h | j);

    return 0;
}