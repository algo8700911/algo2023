#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct timedate 
{
    unsigned char god : 6;
    unsigned char hv : 7;
    unsigned char sec : 7;
    unsigned char weekday : 3;
    unsigned char monthday : 6;
    unsigned char month : 5;
    unsigned char year : 8;
}; 
    

int main()
{
    struct timedate td;
    unsigned char a, b, c, d, e, f, g;

    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №1 лабораторної роботи №1\nВведіть значення годин ");
    scanf("%hhu", &a);
    td.god = a;
    printf("Введіть значення хвилин ");
    scanf("%hhu", &b);
    td.hv = b;
    printf("Введіть значення секунд ");
    scanf("%hhu", &c);
    td.sec = c;
    printf("Введіть значення дня тижня ");
    scanf("%hhu", &d);
    td.weekday = d;
    printf("Введіть значення дня місяця ");
    scanf("%hhu", &e);
    td.monthday = e;
    printf("Введіть значення місяць ");
    scanf("%hhu", &f);
    td.month = f;
    printf("Введіть значення року ");
    scanf("%hhu", &g);
    td.year = g;
    printf("Дата і час введений вами: %d:%d:%d %d.%d.%d %d день тижня\n", td.god, td.hv, td.sec, td.monthday, td.month, td.year, td.weekday);
    printf("Розмір структури %lu\n", sizeof(struct timedate));
    printf("Розмір структури зі стандартної бібліотеки %lu\n", sizeof(struct tm));

    return 0;
}