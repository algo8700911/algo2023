#include <stdio.h>
#include <stdint.h>

union 
{
    struct  
    {
        uint32_t b0 : 1;
        uint32_t b1 : 1;
        uint32_t b2 : 1;
        uint32_t b3 : 1;
        uint32_t b4 : 1;
        uint32_t b5 : 1;
        uint32_t b6 : 1;
        uint32_t b7 : 1;
        uint32_t b8 : 1;
        uint32_t b9 : 1;
        uint32_t b10 : 1;
        uint32_t b11 : 1;
        uint32_t b12 : 1;
        uint32_t b13 : 1;
        uint32_t b14 : 1;
        uint32_t b15 : 1;
        uint32_t b16 : 1;
        uint32_t b17 : 1;
        uint32_t b18 : 1;
        uint32_t b19 : 1;
        uint32_t b20 : 1;
        uint32_t b21 : 1;
        uint32_t b22 : 1;
        uint32_t b23 : 1;
        uint32_t b24 : 1;
        uint32_t b25 : 1;
        uint32_t b26 : 1;
        uint32_t b27 : 1;
        uint32_t b28 : 1;
        uint32_t b29 : 1;
        uint32_t b30 : 1;
        uint32_t znakbit : 1;
    } Struct;
    float Num;
} Union;

int main() 
{
    float a;
    int k = 0;

    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №4 лабораторної роботи №1\nВведіть дійсне число типу float: ");
    scanf("%f", &a);
    Union.Num = a;
    char arr[] = {Union.Struct.b0, Union.Struct.b1, Union.Struct.b2, Union.Struct.b3,Union.Struct.b4, Union.Struct.b5, Union.Struct.b6, Union.Struct.b7, Union.Struct.b8, Union.Struct.b9, Union.Struct.b10, Union.Struct.b11, Union.Struct.b12, Union.Struct.b13, Union.Struct.b14, Union.Struct.b15, Union.Struct.b16, Union.Struct.b17, Union.Struct.b18, Union.Struct.b19, Union.Struct.b20, Union.Struct.b21, Union.Struct.b22, Union.Struct.b23, Union.Struct.b24, Union.Struct.b25, Union.Struct.b26, Union.Struct.b27, Union.Struct.b28, Union.Struct.b29, Union.Struct.b30, Union.Struct.znakbit};
    if (Union.Struct.znakbit == 0)
    {
        printf("Введене число є додатним");
    }
    else 
    {
        printf("Введене число є від'ємним");
    }
    printf("\nЗначення введеного числа побітово: ");
    for (int i = 31; i >= 0; i--)
    {
        printf("%d ", arr[i]);
    }
    printf("\nЗначення введеного числа побайтово: ");
    for (int i = 31; i >= 0; i--)
    {
        printf("%d", arr[i]);
        k++;
        if (k == 8) 
        {
            printf(" ");
            k = 0;
        }
    }
    printf("\nМантиса: ");
    for (int i = 22; i >= 0; i--)
    {
        printf("%d ", arr[i]);
    }
    printf("\nСтупінь: ");
    for (int i = 30; i >= 23; i--)
    {
        printf("%d ", arr[i]);
    }
    printf("\nРозмір користувацької змінної: %lu\n", sizeof(Union));

    return 0;
}