#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int randomInt(int min, int max) {
    return min + rand() % (max - min + 1);
}

void generateArray(int arr[], int size, int min, int max) {
    int i;
    for (i = 0; i < size; i++) {
        arr[i] = randomInt(min, max);
    }
}

void printArray(int arr[], int size) {
    int i;
    for (i = 0; i < size; i++) {
        printf("%d ", arr[i]);
    }
    printf("\n");
}

void zamina(int *a, int *b) {
    int temp = *a;
    *a = *b;
    *b = temp;
}

typedef struct Node {
    int data;
    struct Node* next;
    struct Node* prev;
} Node;

Node* createNode(int data) {
    Node* newNode = (Node*)malloc(sizeof(Node));
    newNode->data = data;
    newNode->next = NULL;
    newNode->prev = NULL;
    return newNode;
}

void insertBegin(Node **head, int data) {
    Node* newNode = createNode(data);
    if (*head == NULL) {
        *head = newNode;
    } else {
        newNode->next = *head;
        (*head)->prev = newNode;
        *head = newNode;
    }
}

void selectionSortList(Node **head) {
    Node *current, *next, *min;
    for (current = *head; current->next != NULL; current = current->next) {
        min = current;
        for (next = current->next; next != NULL; next = next->next) {
            if (next->data < min->data) {
                min = next;
            }
        }
        zamina(&(current->data), &(min->data));
    }
}

void insertionSortArray(int arr[], int size) {
    int i, key, j;
    for (i = 1; i < size; i++) {
        key = arr[i];
        j = i - 1;

        while (j >= 0 && arr[j] > key) {
            arr[j + 1] = arr[j];
            j = j - 1;
        }
        arr[j + 1] = key;
    }
}

void insertionSortList(Node **head) {
    Node *current, *key, *prev;
    if (*head == NULL || (*head)->next == NULL) {
        return;
    }

    current = (*head)->next;
    while (current != NULL) {
        key = current;
        prev = current->prev;
        while (prev != NULL && prev->data > key->data) {
            prev->next = key->next;
            if (key->next != NULL) {
                key->next->prev = prev;
            }
            key->next = prev;
            key->prev = prev->prev;
            if (prev->prev != NULL) {
                prev->prev->next = key;
            } else {
                *head = key;
            }
            prev->prev = key;
            prev = key->prev;
        }
        current = current->next;
    }
}

void SortTiming(int size) {
    int *arr = (int*)malloc(size * sizeof(int));
    clock_t start, end;
    double cpu_time_used;

    generateArray(arr, size, 1, size);

    Node *head = NULL;
    int i;
    for (i = 0; i < size; i++) {
        insertBegin(&head, arr[i]);
    }
    start = clock();
    selectionSortList(&head);
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Сортування вибором з двоспрямованим списком (%d елементів): %f секунд\n", size, cpu_time_used);

    generateArray(arr, size, 1, size);
    start = clock();
    insertionSortArray(arr, size);
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Сортування вставками з масивом (%d елементів): %f секунд\n", size, cpu_time_used);

    head = NULL;
    for (i = 0; i < size; i++) {
        insertBegin(&head, arr[i]);
    }
    start = clock();
    insertionSortList(&head);
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Сортування вставками з двоспрямованим списком (%d елементів): %f секунд\n", size, cpu_time_used);

    free(arr);
}

int main() {
    srand(time(NULL));

    int sizes[] = {10, 100, 500, 1000, 2000, 5000, 10000};
    int numSizes = sizeof(sizes) / sizeof(sizes[0]);
    int i;

    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №1 лабораторної роботи №5\n\n");

    for (i = 0; i < numSizes; i++) {
        printf("Розмірність: %d\n", sizes[i]);
        SortTiming(sizes[i]);
        printf("\n");
    }

    return 0;
}