#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>

#define MAX_VERTICES 19

void DepthFirstSearch(int edges[MAX_VERTICES][MAX_VERTICES], int numVertices, bool visited[MAX_VERTICES], int startVertex, char* cityNames[MAX_VERTICES]);
void BreadthFirstSearch(int edges[MAX_VERTICES][MAX_VERTICES], int numVertices, bool visited[MAX_VERTICES], int startVertex, int km[MAX_VERTICES], char* cityNames[MAX_VERTICES]);

int main() {
    char* cityNames[MAX_VERTICES] = { "Київ", "Житомир", "Новоград-Волинський", "Рівне", "Луцьк",
                                      "Бердичів", "Вінниця", "Хмельницький", "Тернопіль",
                                      "Шепетівка", "Біла Церква", "Умань",
                                      "Черкаси", "Кременчук", "Полтава", "Харків",
                                      "Прилуки", "Суми", "Миргород" };
    
    int km[MAX_VERTICES] = { 135, 80, 100, 68, 38, 73, 110, 104, 115, 78, 115, 146, 105, 181, 130, 128, 175, 109 };
    
    int edges[MAX_VERTICES][MAX_VERTICES] = {
        { 0, 1, 10, 16, 2, 5, 9, 11, 12, 14, 17, 18, 3, 6, 13, 15, 4, 7, 8 },
        { 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        { 10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 },
        // Додайте решту ребер
    };
    
    bool visited[MAX_VERTICES] = { false };
    
    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №1 лабораторної роботи №7-8\n\nDFS\n");
    for (int i = 0; i < MAX_VERTICES; i++) {
        if (!visited[i])
            DepthFirstSearch(edges, MAX_VERTICES, visited, i, cityNames);
    }
    
    memset(visited, false, sizeof(visited));
    
    printf("\nBFS\n");
    for (int i = 0; i < MAX_VERTICES; i++) {
        if (!visited[i])
            BreadthFirstSearch(edges, MAX_VERTICES, visited, i, km, cityNames);
    }
    
    printf("\nВсі можливі маршрути:\n");
    printf("\nКиїв - Житомир: %d\n", km[0]);
    printf("Київ - Житомир - Новоград-Волинський: %d\n", km[0] + km[1]);
    printf("Київ - Житомир - Новоград-Волинський - Рівне: %d\n", km[0] + km[1] + km[2]);
    printf("Київ - Житомир - Новоград-Волинський - Рівне - Луцьк: %d\n", km[0] + km[1] + km[2] + km[3]);
    printf("Київ - Житомир - Бердичів: %d\n", km[0] + km[4]);
    printf("Київ - Житомир - Бердичів - Вінниця: %d\n", km[0] + km[4] + km[5]);
    printf("Київ - Житомир - Бердичів - Вінниця - Хмельницький: %d\n", km[0] + km[4] + km[5] + km[6]);
    printf("Київ - Житомир - Бердичів - Вінниця - Хмельницький - Тернопіль: %d\n", km[0] + km[4] + km[5] + km[6] + km[7]);
    printf("Київ - Житомир - Шепетівка: %d\n", km[0] + km[10]);
    printf("Київ - Біла Церква - Умань: %d\n", km[9] + km[10]);
    printf("Київ - Біла Церква - Черкаси - Кременчук: %d\n", km[9] + km[11] + km[12]);
    printf("Київ - Біла Церква - Полтава - Харків: %d\n", km[9] + km[13] + km[14]);
    printf("Київ - Прилуки - Суми: %d\n", km[15] + km[16]);
    printf("Київ - Прилуки - Миргород: %d\n", km[15] + km[17]);
    
    return 0;
}

void DepthFirstSearch(int edges[MAX_VERTICES][MAX_VERTICES], int numVertices, bool visited[MAX_VERTICES], int startVertex, char* cityNames[MAX_VERTICES]) {
    visited[startVertex] = true;
    printf("%s ", cityNames[startVertex]);
    
    for (int i = 0; i < numVertices; i++) {
        if (i == startVertex)
            continue;
        if (!visited[i] && edges[startVertex][i] == 1) {
            DepthFirstSearch(edges, numVertices, visited, i, cityNames);
        }
    }
}

void BreadthFirstSearch(int edges[MAX_VERTICES][MAX_VERTICES], int numVertices, bool visited[MAX_VERTICES], int startVertex, int km[MAX_VERTICES], char* cityNames[MAX_VERTICES]) {
    int queue[MAX_VERTICES];
    int front = 0;
    int rear = 0;
    
    queue[rear++] = startVertex;
    visited[startVertex] = true;
    
    while (front != rear) {
        int currentVertex = queue[front++];
        printf("%s ", cityNames[currentVertex]);
        
        for (int i = 0; i < numVertices; i++) {
            if (i == startVertex)
                continue;
            if (!visited[i] && edges[currentVertex][i] == 1) {
                queue[rear++] = i;
                visited[i] = true;
            }
        }
    }
}
