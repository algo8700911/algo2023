#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void heapify(double arr[], int n, int i) {
    int largest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < n && arr[left] > arr[largest])
        largest = left;

    if (right < n && arr[right] > arr[largest])
        largest = right;

    if (largest != i) {
        double temp = arr[i];
        arr[i] = arr[largest];
        arr[largest] = temp;

        heapify(arr, n, largest);
    }
}

void HeapSort(double arr[], int n) {
    for (int i = n / 2 - 1; i >= 0; i--)
        heapify(arr, n, i);

    for (int i = n - 1; i >= 0; i--) {
        double temp = arr[0];
        arr[0] = arr[i];
        arr[i] = temp;

        heapify(arr, i, 0);
    }
}

void ShellSort(float arr[], int n) {
    for (int gap = n / 2; gap > 0; gap /= 2) {
        for (int i = gap; i < n; i++) {
            float temp = arr[i];
            int j;
            for (j = i; j >= gap && arr[j - gap] > temp; j -= gap)
                arr[j] = arr[j - gap];
            arr[j] = temp;
        }
    }
}

void CountingSort(char arr[], int n) {
    const int range = -10 - (-100) + 1;

    int count[range] = {0};
    char output[n];

    for (int i = 0; i < n; i++)
        count[arr[i] - (-100)]++;

    for (int i = 1; i < range; i++)
        count[i] += count[i - 1];

    for (int i = n - 1; i >= 0; i--) {
        output[count[arr[i] - (-100)] - 1] = arr[i];
        count[arr[i] - (-100)]--;
    }

    for (int i = 0; i < n; i++)
        arr[i] = output[i];
}

int main() {
    srand(time(NULL));

    int sizes[] = {10, 100, 500, 1000, 2000, 5000, 10000};
    int num_sizes = sizeof(sizes) / sizeof(sizes[0]);

    clock_t start, end;
    double cpu_time_used;

    printf("Білий Микола ІПЗ 22-2(1)\nЗавдання №1 лабораторної роботи №6\n\n");

    for (int i = 0; i < num_sizes; i++) {
        int size = sizes[i];

        double *arr_heap = malloc(size * sizeof(double));
        float *arr_shell = malloc(size * sizeof(float));
        char *arr_counting = malloc(size * sizeof(char));

        for (int j = 0; j < size; j++) {
            arr_heap[j] = -10 + ((double)rand() / RAND_MAX) * (100 - (-10));
            arr_shell[j] = ((float)rand() / RAND_MAX) * (200 - 0);
            arr_counting[j] = -100 + rand() % 91;
        }

        start = clock();
        HeapSort(arr_heap, size);
        end = clock();
        cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
        printf("Розмірність: %d\n", size);
        printf("Сортування пірамідальним методом (%d елементів): %.6f секунд\n", size, cpu_time_used);

        start = clock();
        ShellSort(arr_shell, size);
        end = clock();
        cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
        printf("Сортування Шелла з масивом (%d елементів): %.6f секунд\n", size, cpu_time_used);

        start = clock();
        CountingSort(arr_counting, size);
        end = clock();
        cpu_time_used = ((double)(end - start)) / CLOCKS_PER_SEC;
        printf("Сортування підрахунком з масивом (%d елементів): %.6f секунд\n", size, cpu_time_used);

        free(arr_heap);
        free(arr_shell);
        free(arr_counting);

        printf("\n");
    }

    return 0;
}
